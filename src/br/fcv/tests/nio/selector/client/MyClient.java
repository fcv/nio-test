package br.fcv.tests.nio.selector.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class MyClient {
	
	
	public static final int DEFAULT_PORT = 8000;
	public static final String DEFAULT_HOST = "localhost";
	
	private final String host;
	private final int port;
	private final String id;

	public MyClient(String host, int port, String id) {
		this.host = host;		
		this.port = port;
		this.id = id;
	}
	
	public void start() throws IOException {
		System.out.println("starting client, conecting to " + host + ":" + port);
		SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress(host, port));
		socketChannel.configureBlocking(false);
		System.out.println("connectiong established");
		String msg = "[" +id+  "] My msg #";
				
		for (int i = 1; ; i++) {			
			String currentMsg = msg + i;
			
			System.out.println("sending message: '" + currentMsg + "'");
			ByteBuffer buffer = ByteBuffer.wrap(currentMsg.getBytes());
			socketChannel.write(buffer);
			
			System.out.println("message send, putting thread to sleep");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Thread.interrupted();
			} 
		}
//		socket.close();
	}

}
