package br.fcv.tests.nio.selector.client;

import java.io.IOException;

public class MainClient {
	
	public static void main(String[] args) throws IOException {
		int port = MyClient.DEFAULT_PORT;
		String host = MyClient.DEFAULT_HOST;
		String id = "1";
		
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			
			if (arg.equals("--port") || arg.equals("-p")) {
				if (++i >= args.length) {
					System.out.println("Missing value for parameter 'port'");
					return;
				}
				try {
					port = Integer.parseInt(args[i]);
				} catch (NumberFormatException nfe) {
					System.out.println("Invalid value("+args[i]+") for parameter 'port'");
					return;
				}
			} else if (arg.equals("--host") || arg.equals("-h")) {
				if (++i >= args.length) {
					System.out.println("Missing value for parameter 'host'");
					return;
				}
				host = args[i];
			} else if (arg.equals("--id")) {
				if (++i >= args.length) {
					System.out.println("Missing value for parameter 'host'");
					return;
				}
				id = args[i];
			} else {
				System.out.println("Unknown parameter: " + arg);
				return;
			}
		}
		
		MyClient c = new MyClient(host, port, id);
		c.start();
	}

}
