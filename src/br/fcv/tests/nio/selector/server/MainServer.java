package br.fcv.tests.nio.selector.server;

import java.io.IOException;

public class MainServer {
	
	public static void main(String[] args) throws IOException {
		int port = MyServer.DEFAULT_PORT;
		boolean async = false;
		
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			
			if (arg.equals("--port") || arg.equals("-p")) {
				if (++i >= args.length) {
					System.out.println("Missing value for parameter 'port'");
					return;
				}
				try {
					port = Integer.parseInt(args[i]);
				} catch (NumberFormatException nfe) {
					System.out.println("Invalid value("+args[i]+") for parameter 'port'");
					return;
				}
			} else if (arg.equals("--async")) {
				async = true;
			} else {
				System.out.println("Unknown parameter: " + arg);
				return;
			}
		}

		MyServer s = new MyServer(port, async);
		s.start();
	}

}
