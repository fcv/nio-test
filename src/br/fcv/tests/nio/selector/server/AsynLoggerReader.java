package br.fcv.tests.nio.selector.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class AsynLoggerReader extends LoggerReader {

	private final ExecutorService executor = Executors.newFixedThreadPool(
			Runtime.getRuntime().availableProcessors() * 2, 
			new ThreadFactory() {
				private final AtomicInteger count = new AtomicInteger(0);
				@Override public Thread newThread(Runnable r) {
					return new Thread(r, "AsyncEchoReader-Thread#" + count.incrementAndGet());
				}
			});
	
	@Override
	protected void processMessage(final String message) {
		Runnable run = new Runnable() {
			@Override
			public void run() {
				AsynLoggerReader.super.processMessage(message);
				
			}
			
		};
		executor.submit(run);
	}
}
