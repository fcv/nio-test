package br.fcv.tests.nio.selector.server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoggerReader implements Reader {

	private final Logger log = LoggerFactory.getLogger(LoggerReader.class);
	private final ByteBuffer buffer = ByteBuffer.allocate(2048);
	
	@Override
	public void read(SelectionKey key) throws IOException {
		process(key);
	}
	
	protected final void process(SelectionKey key) {
		this.process(key, log);
	}
	
	protected final void process(SelectionKey key, Logger log) {
		// log.info("entering run()");
		try {
			SocketChannel socketChannel = (SocketChannel) key.channel();
			buffer.clear();
			ByteArrayOutputStream out = new ByteArrayOutputStream(1);
			
			int count;
			try {
				while ((count = socketChannel.read(buffer)) > 0) {
					out.write(buffer.array());
					buffer.clear();
				}
							
				if (count < 0) {
					socketChannel.close();
				} else {
					processMessage( new String(out.toByteArray()) );
				}
			} catch (IOException e) {
				log.error("io exception while reading channel", e); 
			}
		} finally {
			// log.info("exiting run()");
		}
	}
	
	protected void processMessage(String message) {
		log.info("message received: {}", message);
	}
	
}
