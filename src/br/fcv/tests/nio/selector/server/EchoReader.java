/**
 * 
 */
package br.fcv.tests.nio.selector.server;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

class EchoReader implements Reader {
	
	private final ByteBuffer buffer = ByteBuffer.allocate(2048);
	private final OutputStream out;
	
	EchoReader(OutputStream out) {
		this.out = out;
	}
	
	EchoReader() {
		this(System.out);
	}
	

	@Override
	public void read(SelectionKey key) throws IOException {
		System.out.println(" - EchoReader invoke.start");
		SocketChannel socketChannel = (SocketChannel) key.channel();
		Socket socket = socketChannel.socket();
		
		System.out.printf(" - about socket -> closed: %s, connected: %s, inputShutdown: %s, outputShutdown: %s\n", 
				socket.isClosed(),
				socket.isConnected(),
				socket.isInputShutdown(),
				socket.isOutputShutdown());
		
		System.out.println(" - EchoReader remote server: " + socket.getRemoteSocketAddress());
		
		buffer.clear();
		
		BufferedOutputStream bos = 
			new BufferedOutputStream(this.out);
		
		int count;
		try {
			while ((count = socketChannel.read(buffer)) > 0) {
				bos.write(buffer.array());	
				buffer.clear();
			}			
			bos.write('\n');
			bos.flush();
			
			System.out.println(" - RechoReader invoke. last count = " + count);
			
			if (count < 0) {
				socketChannel.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			System.out.println(" - EchoReader invoke.finally");
		}
		System.out.println(" - EchoReader invoke.end");
	}
	
}