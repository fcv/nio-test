package br.fcv.tests.nio.selector.server;

import static java.nio.channels.SelectionKey.OP_ACCEPT;
import static java.nio.channels.SelectionKey.OP_READ;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;


public class MyServer {
	
	public static final int DEFAULT_PORT = 8000;
	
	private final int port;
	private final Reader echo;
	
	public MyServer(int port) {
		this(port, false);
	}
	
	public MyServer(int port, boolean async) {
		this.port = port;
		
		this.echo = async ? new AsynLoggerReader() : new LoggerReader();
	}
	
	public void start() throws IOException {
		System.out.println("starting server");
		ServerSocketChannel serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);
		
		ServerSocket serverSocket = serverChannel.socket();
		System.out.println("bind server to port " + port);
		serverSocket.bind(new InetSocketAddress(port));
		
		Selector selector = Selector.open();
		
		serverChannel.register(selector, OP_ACCEPT);
		
		while (true) {
			System.out.println("calling selector.select()");
			int n = selector.select();
			System.out.println("selector.select() returned with: " + n);
			
			if (n != 0) {

				for (Iterator<SelectionKey> it = selector.selectedKeys().iterator(); 
						it.hasNext(); ) {
					SelectionKey key = it.next();
					
					// new incoming connection
					if (key.isAcceptable()) {
						
						ServerSocketChannel server = (ServerSocketChannel) key.channel();
						SocketChannel socketChannel = server.accept();
						socketChannel.configureBlocking(false);
						socketChannel.register(selector, OP_READ);
						
						System.out.println("new connection accepted");
					}
					
					// new message ready to be read
					if (key.isReadable()) {
						System.out.println("reading from connection");
						readFromKey(key);
					}
					
					// removing the key since it has been handled
					it.remove();
				}
			}
		}
	}
	
	protected void readFromKey(SelectionKey key) throws IOException {
		echo.read(key);
	}

}
