package br.fcv.tests.nio.selector.server;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public interface Reader {
	
	public void read(SelectionKey key) throws IOException;
	

}
